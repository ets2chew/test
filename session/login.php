<?php
session_start();
$pdo = new PDO('mysql:host=localhost;dbname=web278_website', 'web', 'Knorke1337');

if (isset($_GET['login'])) {
    $email = $_POST['email'];
    $passwort = $_POST['passwort'];

    $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
    $result = $statement->execute(array('email' => $email));
    $user = $statement->fetch();
    if ($user !== false && password_verify($passwort, $user['passwort'])) {
        $_SESSION['userid'] = $user['id'];
        die('Login successful. <a href="../index.php">Continue</a> to the website.');
    } else {
        $errorMessage = "E-Mail or password wrong. Please try again. <br />";
    }

}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/login.css" media="screen" rel="stylesheet" type="text/css"/>
    <script src="../js/login.js"></script>
    <title>Login</title>
</head>
<body>

<?php
if (isset($errorMessage)) {
    echo $errorMessage;
}
?>

<div id="login-button">
    <img src="https://dqcgrsy5v35b9.cloudfront.net/cruiseplanner/assets/img/icons/login-w-icon.png">
    </img>
</div>
<div id="container">
    <h1>Sign in</h1>
    <span class="close-btn">
    <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></img>
  </span>

    <form>
        <input type="email" name="email" placeholder="E-Mail">
        <input type="password" name="passwort" placeholder="Password">
        <input type="submit" value="Sign in">
        <div id="remember-container">
            <input type="checkbox" id="checkbox-2-1" class="checkbox" checked="checked"/>
            <span id="remember">Remember me</span>
            <span id="forgotten">Forgotten password</span>
        </div>
    </form>
</div>

<!-- Forgotten Password Container -->
<div id="forgotten-container">
    <h1>Forgotten</h1>
    <span class="close-btn">
    <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></img>
  </span>

    <form>
        <input type="email" name="email" placeholder="E-mail">
        <a href="#" class="orange-btn">Get new password</a>
    </form>
</div>
</body>
</html>