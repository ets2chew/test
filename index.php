<?php
    session_start();
    if (!isset($_SESSION['userid']))
        die('Please <a href="session/login.php">login</a> first!');
    $userid = $_SESSION['userid'];
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="css/base.css" media="screen" rel="stylesheet" type="text/css"/>
    <title>Home</title>
</head>
<body>
    <h1>Thanks for choosing Donut Earth Society!</h1>
</body>
</html>